#Fichier Template pour définir les attributs d'une carte.
#Tu n'as qu'à remplir les informations dans la fonction _ready().
#J'expliquerai quoi mettre où plus bas.
#Les hashtags (#) représentent des débuts de commentaires: tout ce qui vient après sur la meme ligne ne sera pas vu par le code.
#Voici un exemple:
extends "res://Templates/Card.gd" #Cette ligne assure que le programme reconnaise que ce fichier doit être lu avec une carte.

func _ready():
    #Si tu pense ne pas avoir besoin d'un des attributs suivants, laisse les comme ils sont, le programme les prendra automatiquement en charge.
    #Attributs de la carte          #Comment les définir

	nom_ = ""                       #Le nom de la carte doit être mis entre guillemets.
	cout_ = 0                       #Le coût en karma de la carte (0,1,2,etc...)
	flash_ = false                  #Si cette carte peut être utilisée en réponse durant le tour de l'adversaire, mettre true, sinon laisser à false.
	type_= Type.Invocation          #Type de la carte. (Type.Sort, Type.Lieu, Type.Invocation, Type.Relique)
	typeInvocation_ = ""            #Si le type est une Invocation, on peut ajouter une précision entre les guillemets. Joueur d'osselets est par exemple un Revenant. 
	texteCarte = ""                 #Texte de description de la carte entre guillemets. Pour sauter une ligne, remplacer enter par \n . Je vais lire cette partie là spécifiquement pour coder les effets de la carte.
	polarite_ = Polarite.Aucun      #(Polarite.Paix, Polarite.Guerre, ou Polarite.Aucun). Si la carte n'est pas une Invocation, mettre la Polarite à Aucun.
	valeurPolarite_ = 0             #Les points d'attaques (0,1,2,etc...). Si la Polarite de la carte est définie à Aucun, ou que la carte n'est pas une Invocation, laisser à 0.


	connect("card_selected",self,"on_card_clicked")
	get_node("Node").updateCard()
	pass # Replace with function body.

