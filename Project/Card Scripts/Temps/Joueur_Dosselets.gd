extends "res://Templates/Card.gd"

var carteADefausser

func ej():
	carteADefausser = null
	while(carteADefausser == null):
		pass
	player.get_node("Hand").discard(carteADefausser)



func _ready():
	nom_ = "Joueur d'osselets"
	cout_ = 3
	flash_ = true
	type_= Type.Invocation
	typeInvocation_ = "Revenant"
	texteCarte = "Entrée en Jeu: Défaussez une carte de votre main."
	polarite_ = Polarite.Paix
	valeurPolarite_ = 3
	get_node("Node").updateCard()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_card_clicked(carte):
	carteADefausser = carte
	pass # Replace with function body.
