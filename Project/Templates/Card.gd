extends Node2D


enum Type{
	Sort = 0,
	Lieu = 1 ,
	Invocation = 2,
	Relique = 3
	}
enum Polarite{
	Aucun = 0,
	Paix = 1,
	Guerre = 2
	}
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var nom_ = ""
export var cout_ = 0
export var dessin = ""
export var texteCarte = ""
export var flash_ = false

export var type_ = Type.Invocation
export var typeInvocation_ = ""
export var polarite_ = Polarite.Aucun
export var valeurPolarite_ = 0

#var engagee = false
#var attachements = 

signal card_selected(x)
signal card_hovered(y,z)
var player
var selectedZone
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.



func _on_Button_pressed():
	emit_signal("card_selected",self)
	
	pass # Replace with function body.


var entered = false
func _process(_delta):
	inspectCard()

func play():
	selectedZone = null

	if (type_ == Type.Sort):
		.sort()
	if (type_ == Type.Lieu):
		pass
	
	if (type_ == Type.Relique):
		player.reliques.push_back(self)
	
	if (type_ ==  Type.Invocation):
		while(selectedZone == null):
			pass
		selectedZone.cartes.push_back(self)
		.ej()

func _on_zone_selected(zone):
	selectedZone = zone

func inspectCard():
	if get_node("Button").is_hovered() and !entered:
		emit_signal("card_hovered",self, true)
		entered = true
	if !(get_node("Button").is_hovered()) and entered:
		emit_signal("card_hovered",self, false)
		entered = false
	pass
