extends Node
signal selectedZone(x)

var Paix = 0
var Guerre = 0

var cards = []
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func countWarPeace():
	Paix = 0
	Guerre = 0
	
	for i in cards:
		if i.polarite_ == 1:
			Paix += i.valeurPolarite_
		elif i.polarite_ == 2:
			Guerre += i.valeurPolarite_


func display():
	var viscards = $cardsInZone.get_children()
	for i in viscards:
		i.queue_free()
	
	#Might be tweaked later, do not touch
	var scaleCards = Vector2(($Button.rect_size.y),($Button.rect_size.y))*0.00175
	var start = $Button.rect_size.x * 0.15
	var end = ($Button.rect_size.x * 1.08)-(scaleCards.x*360)
	var decal = (end-start)/(cards.size()-1)
	
	for i in cards:
		i.scale = scaleCards
		i.position.x = start
		i.position.y = $Button.rect_size.y * 0.5
		start += decal
		$cardsInZone.add_child(i)
		


func _on_Button_pressed():
	emit_signal("selectedZone",self)
	pass # Replace with function body.
