extends Node

var cardScriptList = [
	"res://Card Scripts/Temps/Joueur_Dosselets.gd",
	"res://Card Scripts/Temps/Joueur_Dosselets.gd",
	"res://Card Scripts/Temps/Joueur_Dosselets.gd",

	"res://Card Scripts/Temps/Joueur_Dosselets.gd",
	"res://Card Scripts/Temps/Joueur_Dosselets.gd",
	"res://Card Scripts/Temps/Joueur_Dosselets.gd"]

#Contains all the cards in the deck
var deck = []

#Takes text file containing the list of cards and creates a appends them to the 
#cardScriptList
func  importDeckFromFile():
	deck.empty()
	pass


func emptyDeckInHand():
	while(!deck.empty()):
		draw()
	get_parent().get_node("Hand").display()


#Creates the cards from the cardScriptList and adds them to the deck.
func createDeck():
	var script
	print("Cards to print:", cardScriptList.size())
	while(!cardScriptList.empty()):
		script = cardScriptList.pop_back()
		deck.push_back(createCard(script))
	print("Deck created. Printed cards:",deck.size())

#Creates a card object, and adds the attributes from the card script
#Returns the card
func createCard(cardScript):
	var card = preload("res://Templates/Card.tscn").instance()
	var cardAttributes = load (cardScript)
	card.set_script(cardAttributes)
	card.player = get_parent()
	card._ready()
	print(card.nom_ ," " ,card)
	connectCard(card)
	return card


func connectCard(card):
	Signaling.connectToPlayer(card,card.player)
	Signaling.connectToBoard(card)
	


# Called when the node enters the scene tree for the first time.
func _ready():
	print("Building deck...")
	importDeckFromFile()
	createDeck()
	shuffle()
	emptyDeckInHand()
	pass # Replace with function body.


#Shuffles the deck
func shuffle():
	randomize()
	deck.shuffle()
	print("Deck has been shuffled.")
	pass

#Draws an amount of cards from the deck
func draw(amount:int = 1):
	for _i in range(0, amount):
		var Hand = get_parent().get_node("Hand")
		Hand.cartes.append(deck.back())
		deck.pop_back()

#Discards an amount of cards from the deck
func discard(amount:int = 1):
	for _i in range(0, amount):
		var discard = get_parent().get_node("Défausse")
		discard.cartes.append(deck.back())
		deck.pop_back()
