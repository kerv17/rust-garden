extends Node

var Karma = 0
var monTour = false
var Fruits = 0

var reliques = []
# Called when the node enters the scene tree for the first time.
func _ready():
	Signaling.Players.push_back(self)
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func startofTurn():
	Karma += 5
	updateKarma()

func _on_card_clicked(card):
	actions = "play"
	SelectedCard = card
	waitForAction()
	
func updateKarma():
	$Karma/RichTextLabel.bbcode_text = "[center]" +str(Karma)
	pass

func endOfTurn():
	$Hand.fillHand()

func discardRelique(relique):
	reliques.erase(relique)
	get_node("Défausse").ajouterADefausse(relique)

var SelectedCard
var actions = ""
var turnPassed = false

func action():
	SelectedCard = null
	turnPassed = false
	actions = ""
	while (SelectedCard == null or !turnPassed):#While loop qui fuckup
		pass#Ignore cette ligne
	return actions

func passTurn():
	turnPassed = true
	actions = "Passe"
	waitForAction()


signal acted
func waitForAction():
	emit_signal("acted")
	pass
